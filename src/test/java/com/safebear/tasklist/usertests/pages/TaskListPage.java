package com.safebear.tasklist.usertests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TaskListPage {
    public TaskListPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "addTask")
    WebElement addTaskfield;


    public void addTask(String taskname) {
        wait.until(ExpectedConditions.visibilityOf(addTaskfield));
        addTaskfield.sendKeys(taskname);
        addTaskfield.sendKeys(Keys.ENTER);
    }

    public boolean checkForTask(String taskname) {

        wait.until(ExpectedConditions.visibilityOf(addTaskfield));
        return driver.findElements(By.xpath("//span[contains(text(),\"" + taskname + "\")]")).size() != 0;

    }
}



